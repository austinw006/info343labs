lab6
====

Assets for INFO343 Lab 6 (Underscore and utilizing scripts)

Clone this repo into your lab folder.  Once you do so, decide
if you are going to keep this a separate repo or merge it into
your labs folder repo.

To keep it its own repo:
	Simply make a new git remote (git remote add <name> <address>)
	that points to a new repo on your Bitbucket account.  Remember
	to send invitations to Jason and me.  Proceed as normal, but
	push to this remote, not origin.

To merge it into your lab repo:
	Delete the .git folder in this folder (inside of lab6) after you
	clone it.  Add, commit and push like normal.
