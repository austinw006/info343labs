var mapDemo = {};
var data = [];

$(document).ready(function () {
	$.ajax({
		url: 'http://128.208.132.98/html/class/spots.php?campus=seattle',
		data: {
			action: 'quicklinks'
		},
		type: 'GET',
		success: ajax_success,
		error: ajax_failure
	});

	mapDemo.mapOptions = {
		center: new google.maps.LatLng(47.655,-122.308),
		zoom: 15
	};

	mapDemo.map = new google.maps.Map(document.getElementById('map-canvas'), mapDemo.mapOptions);

});

function ajax_success(response){
	data = JSON.parse(response);
	mapDemo.markers = [];
	for(var i = 0; i < data.length; i++){
		mapDemo.markers.push(new google.maps.Marker({
			position: new google.maps.LatLng(data[i].location.latitude, data[i].location.longitude),
			title: data[i].name,
			id: data[i].id
		}));
			
		mapDemo.markers[i].setMap(mapDemo.map);
		google.maps.event.addListener(mapDemo.markers[i], 'click', alert_position);
	}
	$('#clear').click(clear_map);
}

function ajax_failure (error) {
	console.log('error');
}

function clear_map() {
	for(var i = 0; i <= data.length; i++){
		mapDemo.markers[i].setMap(null);
	}
}

function alert_position() {
	var i = this.id;
	var x = data[i]['hours-today'];
	alert(x + '       ' + data[i].location.description);
}
